# Generated by Django 2.1.2 on 2019-01-09 09:21

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('cadre_de_vie', '0006_auto_20190109_1021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evenement',
            name='owner',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='association.Association'),
        ),
        migrations.AlterField(
            model_name='newpaper',
            name='date',
            field=models.DateField(default=datetime.datetime(2019, 1, 9, 9, 21, 57, 3119, tzinfo=utc)),
        ),
    ]
