# Generated by Django 2.1.2 on 2018-11-22 23:03

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('cadre_de_vie', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='new',
            name='date',
            field=models.DateField(default=datetime.datetime(2018, 11, 22, 23, 3, 47, 337298, tzinfo=utc)),
        ),
    ]
