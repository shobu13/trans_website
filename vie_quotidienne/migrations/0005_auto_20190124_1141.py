# Generated by Django 2.1.5 on 2019-01-24 10:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vie_quotidienne', '0004_auto_20190108_1759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marchehoraire',
            name='marche',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='horaires', to='vie_quotidienne.Marche'),
        ),
    ]
