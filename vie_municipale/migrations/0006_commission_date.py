# Generated by Django 2.1.2 on 2019-01-08 10:49

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('vie_municipale', '0005_conseil_service'),
    ]

    operations = [
        migrations.AddField(
            model_name='commission',
            name='date',
            field=models.DateField(default=datetime.datetime(2019, 1, 8, 10, 49, 48, 753745, tzinfo=utc)),
        ),
    ]
