# Generated by Django 2.1.2 on 2019-01-08 16:57

import datetime
from django.conf import settings
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('vie_municipale', '0006_commission_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commission',
            name='date',
            field=models.DateField(default=datetime.datetime(2019, 1, 8, 16, 57, 32, 237941, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='commission',
            name='suppleants',
            field=models.ManyToManyField(related_name='commission_suppleants', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='commission',
            name='titulaires',
            field=models.ManyToManyField(related_name='commission_titulaire', to=settings.AUTH_USER_MODEL),
        ),
    ]
